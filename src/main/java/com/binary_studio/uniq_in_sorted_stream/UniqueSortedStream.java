package com.binary_studio.uniq_in_sorted_stream;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		// TODO: Ваш код тут. Помните про терминальные операции! :)

		Set<Row<T>> set = new HashSet<>();

		return stream.filter(row -> {
			boolean isExist = set.contains(row);
			set.add(row);
			return !isExist;
		});

	}

}
