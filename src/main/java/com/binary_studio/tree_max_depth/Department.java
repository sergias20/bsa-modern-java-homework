package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Department {

	public final String name;

	public final List<Department> subDepartments;

	public Department(String name) {
		this.name = name;
		this.subDepartments = new ArrayList<>();
	}

	public Department(String name, Department... departments) {
		this.name = name;
		this.subDepartments = Arrays.asList(departments);
	}

	@Override
	public String toString() {
		return "Department{" + "name='" + this.name + '\'' + ", subDepartments=" + this.subDepartments + '}';
	}

	public static void main(String[] args) {
		Department d = new Department("A", new Department("B", null, null, null, null),
				new Department("C", null, null, null, new Department("C1", null, null, new Department("C1-1", new Department("C2-1")))),
				new Department("D"));

		System.out.println(" >>>>>>>>> " + DepartmentMaxDepth.calculateMaxDepth(d));
	}

}
