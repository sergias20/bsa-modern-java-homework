package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Queue;

public final class DepartmentMaxDepth {


	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {

		// Recursion
//		if (rootDepartment == null) {
//			return 0;
//		}
//
//		int max = 0;
//		for (Department sD : rootDepartment.subDepartments) {
//
//			max = Math.max(max, calculateMaxDepth(sD));
//		}
//
//		return max+1;


		// BFS
		int count = 0;

		if (rootDepartment == null) {
			return count;
		}

		Queue<Department> queue = new LinkedList<>();
		queue.offer(rootDepartment);

		while (!queue.isEmpty()) {

			int level = queue.size();
			count++;

			for (int i = 0; i < level; i++) {
				for (Department sD : queue.poll().subDepartments) {
					if (sD != null) {
						queue.offer(sD);
					}
				}
			}

		}

		return count;
	}

}
